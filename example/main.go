package main

import (
	"context"
	"fmt"
	"log"

	repository "gitlab.com/pedroalvesk/mongodb-repository"
	"gitlab.com/pedroalvesk/mongodb-repository/example/models"
)

func main() {
	db := "mongo-embed-v2"
	uri := "mongodb+srv://bolzen:UuuKzOIbWiPm3KQi@cluster0.e2qjo.mongodb.net/"

	repo, err := repository.NewRepository(repository.NewConfig(uri, db, ""))
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	g1 := models.NewGender('F', "Female")
	if err = repo.Create(ctx, g1); err != nil {
		log.Fatalln("failed to create gender:", err)
	}

	g2 := models.NewGender('M', "Male")
	if err = repo.Create(ctx, g2); err != nil {
		log.Fatalln("failed to create gender:", err)
	}

	p1 := models.NewPerson("Person #A", 21, g1)
	if err = repo.Create(ctx, p1); err != nil {
		log.Fatalln("failed to create person:", err)
	}

	p2 := models.NewPerson("Person #B", 42, &models.Gender{ID: g2.ID})
	if err = repo.Create(ctx, p2); err != nil {
		log.Fatalln("failed to create person:", err)
	}

	c1 := models.NewClub("Club#A", []*models.Person{p1, p2})
	if err = repo.Create(ctx, c1); err != nil {
		log.Fatalln("failed to create club:", err)
	}

	c2 := models.NewClub("Club#B", []*models.Person{p2})
	if err = repo.Create(ctx, c2); err != nil {
		log.Fatalln("failed to create club:", err)
	}

	country := models.NewCountry("Country #A", p1, p2)
	if err = repo.Create(ctx, country); err != nil {
		log.Fatalln("failed to create club:", err)
	}

	var dbCountry models.Country
	if err = repo.GetByID(ctx, country.ID, &dbCountry); err != nil {
		log.Fatalln("failed to get club:", err)
	}

	persons := make([]*models.Person, 0)
	if err = repo.Fetch(ctx, &models.Person{}, &persons, repository.Filter{Key: "name", Value: "Person #A"}); err != nil {
		log.Fatalln("failed to get persons:", err)
	}
	fmt.Println(persons)

	clubs := make([]*models.Club, 0)
	if err = repo.Fetch(ctx, &models.Club{}, &clubs, repository.Filter{Key: "name", Value: "Club#A"}); err != nil {
		log.Fatalln("failed to get clubs:", err)
	}
	fmt.Println(clubs)
}
