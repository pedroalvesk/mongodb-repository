package models

type PersonMin struct {
	ID   string `bson:"_id,omitempty"`
	Name string
}

func (p *PersonMin) GetID() string {
	return p.ID
}

func (p *PersonMin) SetID(id string) {
	p.ID = id
}
