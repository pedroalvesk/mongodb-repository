package models

import "fmt"

type Person struct {
	ID     string `bson:"_id,omitempty"`
	Name   string
	Age    uint
	Gender *Gender `mongodb-embed:""`
}

func (p *Person) GetID() string {
	return p.ID
}

func (p *Person) SetID(id string) {
	p.ID = id
}

func (p *Person) GetCollection() string {
	return "persons"
}

func (p *Person) String() string {
	return fmt.Sprintf("%s - %d - %s", p.Name, p.Age, p.Gender)
}

func NewPerson(name string, age uint, gender *Gender) *Person {
	return &Person{
		Name:   name,
		Age:    age,
		Gender: gender,
	}
}
