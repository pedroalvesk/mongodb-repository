package models

import (
	"fmt"
)

type Club struct {
	ID      string `bson:"_id,omitempty"`
	Name    string
	Members []*PersonMin `mongodb-embed:"persons"`
}

func (c *Club) GetID() string {
	return c.ID
}

func (c *Club) SetID(id string) {
	c.ID = id
}

func (c *Club) GetCollection() string {
	return "clubs"
}

func (c *Club) String() string {
	return fmt.Sprintf("(%s)-%s", c.Name, c.Members)
}

func NewClub(name string, members []*Person) *Club {
	m := make([]*PersonMin, 0, len(members))
	for _, mem := range members {
		m = append(m, &PersonMin{ID: mem.ID})
	}

	return &Club{
		Name:    name,
		Members: m,
	}
}

func (c *Club) PrintMembers() {
	for i := range c.Members {
		fmt.Println(i, c.Members[i])
	}
}
