package models

import "fmt"

type Gender struct {
	ID          string `bson:"_id,omitempty"`
	Description string
	Letter      rune
}

func (g *Gender) GetID() string {
	return g.ID
}

func (g *Gender) SetID(id string) {
	g.ID = id
}

func (g *Gender) GetCollection() string {
	return "genders"
}

func (g *Gender) String() string {
	return fmt.Sprintf("(%c)-%s", g.Letter, g.Description)
}

func NewGender(letter rune, description string) *Gender {
	return &Gender{
		Description: description,
		Letter:      letter,
	}
}
