package models

import "fmt"

type Embedded struct {
	Members []*Person `mongodb-embed:""`
}

type Country struct {
	ID    string `bson:"_id,omitempty"`
	Name  string
	Other Embedded `json:"other"`
}

func (c *Country) GetID() string {
	return c.ID
}

func (c *Country) SetID(id string) {
	c.ID = id
}

func (c *Country) GetCollection() string {
	return "countries"
}

func (c *Country) String() string {
	return fmt.Sprintf("%s - %s - %s", c.ID, c.Name, c.Other.Members)
}

func NewCountry(name string, persons ...*Person) *Country {
	return &Country{
		Name:  name,
		Other: Embedded{Members: persons},
	}
}
